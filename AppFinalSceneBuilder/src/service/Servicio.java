package service;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import controller.PokemonNotFoundException;
import modelo.Entrenador;
import modelo.Pokemon;

public class Servicio {

	private MongoUtil mongoUtil = new MongoUtil();

	public Boolean login(String username, String password) throws LoginDenegadoException {

		List<Entrenador> entrenadorcitos = new ArrayList<>();
		MongoDatabase db = mongoUtil.getDatabase();
		MongoCollection<Entrenador> entrenadores = db.getCollection("entrenador", Entrenador.class);

		Bson filtrito = Filters.and(Filters.eq("nombre", username), Filters.eq("password", password));

		FindIterable<Entrenador> it = entrenadores.find(filtrito);
		MongoCursor<Entrenador> cursor = it.cursor();

		while (cursor.hasNext()) {

			Entrenador e = cursor.next();

			entrenadorcitos.add(e);

		}

		if (entrenadorcitos.size() == 0) {

			return false;

		} else if (!(entrenadorcitos.size() == 0)) {

			return true;

		}

		return false;

	}

	public void register(String username, String password) throws LoginDenegadoException {

		MongoDatabase db = mongoUtil.getDatabase();
		MongoCollection<Entrenador> entrenadores = db.getCollection("entrenador", Entrenador.class);

		Entrenador e = new Entrenador();
		e.setNombre(username);
		e.setPassword(password);

		entrenadores.insertOne(e);

	}

	public List<Pokemon> consultarPokemons(String filtro) throws PokemonNotFoundException {

		List<Pokemon> pokemitos = new ArrayList<>();
		MongoDatabase db = mongoUtil.getDatabase();
		MongoCollection<Pokemon> pokemons = db.getCollection("pokemon", Pokemon.class);

		Bson filtrito = Filters.eq("tipo", filtro);

		FindIterable<Pokemon> it = pokemons.find(filtrito);
		MongoCursor<Pokemon> cursor = it.cursor();

		while (cursor.hasNext()) {

			Pokemon p = cursor.next();

			pokemitos.add(p);

		}

		return pokemitos;

	}

	public List<Pokemon> consultarTodosPokemons() throws PokemonNotFoundException {

		List<Pokemon> pokemitos = new ArrayList<>();
		MongoDatabase db = mongoUtil.getDatabase();
		MongoCollection<Pokemon> pokemons = db.getCollection("pokemon", Pokemon.class);

		FindIterable<Pokemon> it = pokemons.find();
		MongoCursor<Pokemon> cursor = it.cursor();

		while (cursor.hasNext()) {

			Pokemon p = cursor.next();

			pokemitos.add(p);

		}

		return pokemitos;

	}

	public void registrarPokeNuevo(String username, String tipo) throws PokemonNotCreatedException {

		MongoDatabase db = mongoUtil.getDatabase();
		MongoCollection<Pokemon> pokemons = db.getCollection("pokemon", Pokemon.class);

		Pokemon p = new Pokemon();
		p.setNombre(username);
		p.setTipo(tipo);

		pokemons.insertOne(p);

	}

	public Servicio() {

	}

}

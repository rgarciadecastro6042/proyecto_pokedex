package service;

public class PokemonNotCreatedException extends Exception {

	private static final long serialVersionUID = -3420140077573079271L;

	public PokemonNotCreatedException() {
	}

	public PokemonNotCreatedException(String message) {
		super(message);
	}

	public PokemonNotCreatedException(Throwable cause) {
		super(cause);
	}

	public PokemonNotCreatedException(String message, Throwable cause) {
		super(message, cause);
	}

	public PokemonNotCreatedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}

package controller;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Pokemon;
import service.Servicio;

public class TablaController extends AppController {

	@FXML
	private TableColumn<Pokemon, String> tipo;

	@FXML
	private TableColumn<Pokemon, String> nombre;

	@FXML
	private TableView<Pokemon> tabla;

	@FXML
	private Button consultar;

	@FXML
	private Button nuevo;

	@FXML
	private Button Salir;

	@FXML
	private ComboBox<String> combo;

	private ObservableList<Pokemon> datos;

	private Servicio service = new Servicio();

	public void consultar(ActionEvent event) {

		try {

			if (combo.getSelectionModel().getSelectedItem() == null) {

				List<Pokemon> resultados = service.consultarTodosPokemons();
				datos.setAll(resultados);

			} else {

				List<Pokemon> resultados = service
						.consultarPokemons(combo.getSelectionModel().getSelectedItem().toString());
				datos.setAll(resultados);

			}

		} catch (PokemonNotFoundException e) {

			datos.clear();

		}

	}

	public void nuevo(ActionEvent event) {

		cambiarVista(FxmlPaths.FXML_PANTALLA_NUEVO);

	}
	
	@FXML
    void exit(ActionEvent event) {

		System.exit(0);		
		
    }

	public void initialize() {

		PropertyValueFactory<Pokemon, String> factoryTipo = new PropertyValueFactory<>("tipo");
		PropertyValueFactory<Pokemon, String> factoryNombre = new PropertyValueFactory<>("nombre");

		tipo.setCellValueFactory(factoryTipo);
		nombre.setCellValueFactory(factoryNombre);

		datos = FXCollections.observableArrayList();
		tabla.setItems(datos);

		combo.getItems().add("acero");
		combo.getItems().add("agua");
		combo.getItems().add("bicho");
		combo.getItems().add("dragon");
		combo.getItems().add("electrico");
		combo.getItems().add("fantasma");
		combo.getItems().add("fuego");
		combo.getItems().add("hada");
		combo.getItems().add("hielo");
		combo.getItems().add("lucha");
		combo.getItems().add("normal");
		combo.getItems().add("planta");
		combo.getItems().add("psiquico");
		combo.getItems().add("roca");
		combo.getItems().add("siniestro");
		combo.getItems().add("tierra");
		combo.getItems().add("veneno");
		combo.getItems().add("volador");

	}

	public TablaController() {

	}

}

package controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class App extends Application {

	private static final Logger log = (Logger) LogManager.getLogger();

	@Override
	public void start(Stage primaryStage) throws Exception {


		AppController controller = new AppController(primaryStage);

		controller.cambiarVista(FxmlPaths.FXML_LOGIN);
		primaryStage.initStyle(StageStyle.UNDECORATED);

		primaryStage.show();
		
		log.log(Level.DEBUG, "Iniciando app");

	}

	public static void main(String[] args) {

		launch();

	}

}

package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;

public class MenuController extends AppController {

	@FXML
	private Button consultar;

	@FXML
	void logout(ActionEvent event) {

		cambiarVista(FxmlPaths.FXML_LOGIN);

	}

	@FXML
	void salir(ActionEvent event) {

		System.exit(0);

	}

	@FXML
	void consultar(ActionEvent event) {

		cambiarVista(FxmlPaths.FXML_PANTALLA_TABLA);

	}

	@FXML
	void info(ActionEvent event) {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setHeaderText(null);
		alert.setTitle("Help");
		alert.setContentText(
				"Esta aplicancion funciona como una pokedex, podemos consultar pokemon por su tipo o nombre y tambien podemos añadir si encontramos alguno nuevo");
		alert.showAndWait();

	}

	public MenuController() {

	}

}

package controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import service.Servicio;

public class LoginController extends AppController {

	private static final Logger log = (Logger) LogManager.getLogger();

	@FXML
	private PasswordField campoContraseña;

	@FXML
	private Button Exit;

	@FXML
	private TextField campoNombre;

	@FXML
	private Button enter;

	private Servicio service = new Servicio();

	@FXML
	void enter(ActionEvent event) {

		try {

			if (service.login(campoNombre.getText(), campoContraseña.getText())) {

				cambiarVista(FxmlPaths.FXML_PANTALLA_MENU);

			} else {

				Alert alert = new Alert(AlertType.ERROR);
				alert.setHeaderText(null);
				alert.setTitle("Usuario o Contraseña Incorrecto");
				alert.setContentText("Usuario o Contraseña introducido incorrectos");
				alert.showAndWait();

				log.log(Level.ERROR, "Error iniciando sesion");

			}

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	@FXML
	void Exit(ActionEvent event) {

		System.exit(0);

	}

	@FXML
	void registrar(ActionEvent event) {

		cambiarVista(FxmlPaths.FXML_REGISTER);

	}

}

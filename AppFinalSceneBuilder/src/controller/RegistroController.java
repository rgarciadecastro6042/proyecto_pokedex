package controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import service.LoginDenegadoException;
import service.Servicio;

public class RegistroController extends AppController {

	private static final Logger log = (Logger) LogManager.getLogger();

	@FXML
	private PasswordField campoContraseña;

	@FXML
	private TextField campoNombre;

	@FXML
	private Button enter;

	private Servicio service = new Servicio();

	public void registrarEntrenador() {

		try {

			service.register(campoNombre.getText().toString(), campoContraseña.getText().toString());
			cambiarVista(FxmlPaths.FXML_LOGIN);

			log.log(Level.DEBUG, "Registrando entrenador");

		} catch (LoginDenegadoException e) {

			e.printStackTrace();
			log.log(Level.ERROR, "Error registrando entrenador");

		}

	}

	public RegistroController() {

	}

}

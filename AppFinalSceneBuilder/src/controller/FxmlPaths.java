package controller;

public class FxmlPaths {

	private static final String PATH_BASE = "/fxml/";
	public static final String FXML_LOGIN = PATH_BASE + "login.fxml";
	public static final String FXML_REGISTER = PATH_BASE + "registro.fxml";
	public static final String FXML_PANTALLA_MENU = PATH_BASE + "menu.fxml";
	public static final String FXML_PANTALLA_TABLA = PATH_BASE + "tabla.fxml";
	public static final String FXML_PANTALLA_NUEVO = PATH_BASE + "nuevo.fxml";

}

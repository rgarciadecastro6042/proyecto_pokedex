package controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import modelo.Pokemon;
import service.PokemonNotCreatedException;
import service.Servicio;

public class NuevoPokeController extends AppController {

	private static final Logger log = (Logger) LogManager.getLogger();

	private Servicio service = new Servicio();

	@FXML
	private Button guardar;

	@FXML
	private TextField nombre;

	@FXML
	private ComboBox<String> combo;

	private Pokemon poke = new Pokemon();

	public void guardar(ActionEvent event) throws PokemonNotCreatedException {

		try {

			if (!nombre.getText().toString().isBlank() && !combo.getSelectionModel().isSelected(0)) {

				poke.setNombre(nombre.getText());
				poke.setTipo(combo.getSelectionModel().getSelectedItem().toString());

				service.registrarPokeNuevo(poke.getNombre(), poke.getTipo());

				cambiarVista(FxmlPaths.FXML_PANTALLA_TABLA);

			} else {

				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setHeaderText(null);
				alert.setTitle("Error");
				alert.setContentText("campos obligatorios");
				alert.showAndWait();
				
				log.log(Level.ERROR, "Error, No hay nombre seleccionado para el pokemon");

			}

		} catch (PokemonNotCreatedException e) {

			throw new PokemonNotCreatedException("Error al crear el pokemon");

		} catch (NullPointerException e) {

			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setHeaderText(null);
			alert.setTitle("Error");
			alert.setContentText("No hay tipo seleccionado para el pokemon");
			alert.showAndWait();
			
			log.log(Level.ERROR, "Error, No hay tipo seleccionado para el pokemon");
			
			throw new PokemonNotCreatedException("No hay tipo seleccionado para el pokemon");

		}

	}

	public void initialize() {

		combo.getItems().add("acero");
		combo.getItems().add("agua");
		combo.getItems().add("bicho");
		combo.getItems().add("dragon");
		combo.getItems().add("electrico");
		combo.getItems().add("fantasma");
		combo.getItems().add("fuego");
		combo.getItems().add("hada");
		combo.getItems().add("hielo");
		combo.getItems().add("lucha");
		combo.getItems().add("normal");
		combo.getItems().add("planta");
		combo.getItems().add("psiquico");
		combo.getItems().add("roca");
		combo.getItems().add("siniestro");
		combo.getItems().add("tierra");
		combo.getItems().add("veneno");
		combo.getItems().add("volador");

	}

}

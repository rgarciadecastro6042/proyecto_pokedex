package modelo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

	private static Config instance;

	private Properties properties;

	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		return instance;
	}

	private Config() {
		properties = new Properties();
		InputStream is = getClass().getResourceAsStream("/config/config.properties");
		try {
			properties.load(is);
		} catch (IOException e) {
			throw new RuntimeException("Error grave. No se ha podido cargar la configuración", e);
		}
	}

	public String getPropiedad(String nombre) {
		return properties.getProperty(nombre);
	}

	public Boolean getPropiedadAsBoolean(String nombre) {
		return Boolean.parseBoolean(getPropiedad(nombre));
	}

	public Integer getPropiedadAsInteger(String nombre) {
		return Integer.parseInt(getPropiedad(nombre));
	}

}

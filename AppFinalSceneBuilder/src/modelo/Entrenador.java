package modelo;

public class Entrenador {

	private String nombre;
	private String password;

	public Entrenador() {

	}
	
	public Entrenador(String nombre, String password) {

		this.nombre = nombre;
		this.password = password;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Entrenador [nombre=" + nombre + ", password=" + password + "]";
	}

}
